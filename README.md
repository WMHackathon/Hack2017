## Pre-reqs
Ensure latest versions of Node and npm
```sh
$ node -v
```
> greater than or equal to v6.9.2

```sh
$ npm -v
```
> greater than or equal to 4.2.0

## Install

```sh
$ npm install webpack-dev-server -g
$ npm install 
```
Installing webpack-dev-server globally may require sudo/run as admin 

## Build and run
```sh
$ webpack-dev-server --port 3000 
```


## Resources

Setup based on https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html

Other React JS resources
* https://facebook.github.io/react/
* https://egghead.io/courses#technology-react - video tutorial courses
* https://github.com/mikechau/react-primer-draft - missing ES6 classes, but good primer on basics

ES6 resources
* http://es6-features.org/#Constants
* https://webapplog.com/es6/
