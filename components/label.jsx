import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom'

class Label extends React.Component {
	constructor(props) {
		super(props);
		this.buttonClick = this.buttonClick.bind(this);
	};
	buttonClick(e) {
		//console.log(e.target.id);	
		window.location = this.props.linkUrl
	}
	render() {
	    return <label style={this.props.styles.label} htmlFor={this.props.labelId}>
				<Link to={this.props.linkUrl} role="button" style={this.props.styles.anchor} id={this.props.labelId} >{this.props.labelName}</Link>
				{this.props.defaultValue}
			</label>

	}
}
export default Label