import React from 'react';
import ReactDOM from 'react-dom';

class Label extends React.Component {
	constructor(props) {
		super(props);
		this.buttonClick = this.buttonClick.bind(this);
	};
	buttonClick(e) {
		//console.log(e.target.id);	
		this.props.clickHandler(e.target.id);
	}
	render() {
	    return <label style={this.props.styles.label} htmlFor={this.props.labelId}>
				<button style={this.props.styles.button} id={this.props.labelId} onClick={this.buttonClick}>{this.props.labelName}</button>
				{this.props.defaultValue}
			</label>

	}
}
export default Label