import React from 'react';
import ReactDOM from 'react-dom';

export default class Dropdown extends React.Component {
	render() {
		return <select id={this.props.selectId}>{this.props.children}</select>
	}
}
