import React from 'react';
import ReactDOM from 'react-dom';
import Button from './button.jsx';
import Dropdown from './dropdown.jsx';
import { Table } from 'react-bootstrap';
import json from './../json/landing-page-home-office.json';

export default class Menu extends React.Component {
	constructor(props){
        super(props);
		this.handleClick = this.handleClick.bind(this);

        this.state = {
            region: ''
        }
    }

	handleClick() {
		var region = document.getElementById("region-select").value;
		this.setState({region});
	}

	getRegions() {
		return Object.keys(json.landingPage).map(function(item) {
			return <option key={item} value={item}>{item}</option>;
		});
	}

	getTableHeaders() {
		var selectedRegion = this.state.region;
		var data = json.landingPage[selectedRegion] || {};
		var rows = Object.keys(data);

		return rows.map((row) => {
			return Object.keys(data[row]).map((header, index) => {
				return <th key={index}>{header}</th>;
			});
		})[0];
	}

	getTableData() {
		var selectedRegion = this.state.region;
		var data = json.landingPage[selectedRegion] || {};
		var rows = Object.keys(data);

		var columns = rows.map((row, index) => {
			var column = data[row];
			var headers = Object.keys(column);

			var items = headers.map((key, index) => {
				return <td key={index}>{column[key]}</td>;
			});
			return <tr key={index}>
				<td>{row}</td>
				{items}
			</tr>;
		});

		return columns;
	}

	render() {
		return <div>
			<Dropdown selectId="region-select" children={this.getRegions()} />
			<Button handleClick={this.handleClick} style="primary" title="Daily"/>
			<Button handleClick={this.handleClick} style="success" title="WTD"/>
			<Button handleClick={this.handleClick} style="danger" title="MTD"/>

			<Table responsive>
				<thead><th></th>{this.getTableHeaders()}</thead>
				<tbody>{this.getTableData()}</tbody>
			</Table>
		</div>
	}
}
