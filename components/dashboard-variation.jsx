import React from 'react';
import ReactDOM from 'react-dom';
import data from '../json/dashboard.json';
import Label from './label-variation.jsx';

let styles = {
	label: {
		display: 'block'
	},
	button: {
		margin: '0 10px 20px 0',
		width: '100px',
		height: '30px',
	}
};

let props = {
	full: 20,
	fast: 30,
	fresh: 40,
	shrink: 50
};

var getData = () => data;

class DashboardVariation extends React.Component {
	constructor() {
		super();
		this.state = {
			full: props.full,
			fast: props.fast,
			fresh: props.fresh,
			shrink: props.shrink
		};
		this.updatePercent=this.updatePercent.bind(this);
	}
	updatePercent(button) {
		//console.log('updatePercent');
		this.setState((prevState, props) => {
			var obj = {};
			var key = (button === "shrink") ? "store" : "customer";
				obj[button] = getData()[key][button]["score"];
			return obj;
		});
	}
 	render() {
	    return <div>
				<h1>Dashboard Variation</h1>
				<p></p>
				<h4>The Customer</h4>
				<hr/>
				<Label clickHandler={this.updatePercent} labelId="full" labelName="Full" defaultValue={this.state.full} styles={styles} />
				<Label clickHandler={this.updatePercent} labelId="fast" labelName="Fast" defaultValue={this.state.fast} styles={styles} />
				<Label clickHandler={this.updatePercent} labelId="fresh" labelName="Fresh" defaultValue={this.state.fresh} styles={styles} />

				<h4>The Store</h4>
				<hr/>
				<Label clickHandler={this.updatePercent} labelId="shrink" labelName="Shrink" defaultValue={this.state.shrink} styles={styles} />
			</div>
 	}
}

export default DashboardVariation