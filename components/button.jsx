import React from 'react';
import ReactDOM from 'react-dom';
import { Button as BootstrapButton } from 'react-bootstrap';

export default class Button extends React.Component {
	constructor(props) {
		super(props);
		this.buttonClick = this.buttonClick.bind(this);
	};
	buttonClick(e) {
		//console.log(e.target.id);	
		this.props.handleClick();
	}	
	render() {
		return <BootstrapButton bsStyle={this.props.style} onClick={this.buttonClick}>{this.props.title}</BootstrapButton>
	}
}
