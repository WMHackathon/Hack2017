import React from 'react';
import ReactDOM from 'react-dom';
import data from '../json/dashboard.json';
import Label from './label.jsx';

let styles = {
	label: {
		display: 'block',
		clear: 'left'
	},
	anchor: {
		display: 'inline-block',
		border: '1px solid #AFAFAF',
		backgroundColor: '#E3E3E3',
		backgroundImage: 'linear-gradient(#E3E3E3, #F7F7F7)',
		textAlign: 'center',
		margin: '0 10px 20px 0',
		padding: '9px 0 22px 0',
		width: '100px',
		height: '22px',
		clear: 'left',
		color: '#000000',
		fontSize: '12px',
		textDecoration: 'none',
		fontFamily: 'Arial'
	}
};


var getData = () => data;

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		console.log(getData());
		this.state = {
			full: getData()["customer"]["full"]["score"],
			fast: getData()["customer"]["fast"]["score"],
			fresh: getData()["customer"]["fresh"]["score"],
			shrink: getData()["store"]["shrink"]["score"]
		};
	}
 	render() {
	    return <div>
				<h1>Dashboard</h1>
				<p></p>
				<h4>The Customer</h4>
				<hr/>
				<Label linkUrl="/full-details" labelId="full" labelName="Full" defaultValue={this.state.full} styles={styles} />
				<Label linkUrl="/full-details" labelId="fast" labelName="Fast" defaultValue={this.state.fast} styles={styles} />
				<Label linkUrl="/full-details" labelId="fresh" labelName="Fresh" defaultValue={this.state.fresh} styles={styles} />

				<h4>The Store</h4>
				<hr/>
				<Label linkUrl="/full-details" labelId="shrink" labelName="Shrink" defaultValue={this.state.shrink} styles={styles} />
			</div>
 	}
}
