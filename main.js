import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import Dashboard from './components/dashboard.jsx'
import DashboardVariation from './components/dashboard-variation.jsx'
import FullDetails from './components/full-details.jsx'

const App = () => (
  <Router>
    <div>
      <ul>
        <li><Link to="/">Dashboard</Link></li>
        <li><Link to="/homepage-variation">Dashboard variaton</Link></li>
        <li><Link to="/full-details">Full Details</Link></li>
      </ul>

      <hr/>

      <Route exact path="/" component={Dashboard}/>
      <Route exact path="/homepage-variation" component={DashboardVariation}/>
      <Route exact path="/full-details" component={FullDetails}/>
    </div>
  </Router>
)

ReactDOM.render(<App />, document.getElementById('root'));